package juego;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Genera los bloques para el juego en base al numero de nivel y el esquema de
 * este
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 */
public class GeneradorNivel {
	private int x = 0;
	private int y = 0;
	private Juego juego;
	private ArrayList<Bloque> bloques;
	
	/**
	 * Genera un nivel en base a un archivo de texto
	 * @param game Juego en el cual se va a generar el nivel
	 * @param nivel archivo de texto que contiene el esquema del nivel
	 */
	public GeneradorNivel(Juego game, InputStream nivel) {
		bloques = new ArrayList<Bloque>();
		try {
			Scanner sc = new Scanner(nivel);
			for (int i = 0; i < 10; i++) {
				x = 0;
				String linea = sc.nextLine();
				for (int j = 0; j < 10; j++) {
					switch (linea.charAt(j)) {
					case 'A':
						bloques.add(new Bloque(game, x, y, 1));
						break;
					case 'B':
						bloques.add(new Bloque(game, x, y, 2));
						break;
					case 'C':
						bloques.add(new Bloque(game, x, y, 3));
						break;
					}
					x += 80;
				}
				y += 20;
			}
			sc.close();
		} catch (IndexOutOfBoundsException e1) {
			System.err.println("Problema al cargar nivel");
		}
	}

	public ArrayList<Bloque> getBloques() {
		return bloques;
	}
}
