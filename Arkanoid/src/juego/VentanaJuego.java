package juego;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 * Controla la ventana de juego y eventos como el fin de juego ,la puntuacion y
 * las vidas
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 * @see Juego
 */
public class VentanaJuego extends JFrame implements ActionListener {
	private Juego game;
	private Timer tJugar;
	private JMenuBar mBarra;
	private JMenu mMenu;
	private JMenuItem miOpciones;
	private JMenuItem miSalir;
	MenuPrincipal menu;
	private int vidas = 3;
	private int nivel = 1;
	private int puntuacion;
	ArrayList<String> puntuaciones;
	private String nombre;
	/**
	 * Genera la ventana de juego
	 * @param menu menu del cual se genera la ventana
	 */
	public VentanaJuego(MenuPrincipal menu) {
		super("Arkanoid");
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.menu = menu;

		mBarra = new JMenuBar();

		mMenu = new JMenu("Menu");
		mMenu.setMnemonic('M');

		miOpciones = new JMenuItem("Opciones");
		miOpciones.setMnemonic('O');
		miOpciones.addActionListener(this);

		miSalir = new JMenuItem("Salir");
		miSalir.setMnemonic('S');
		miSalir.addActionListener(this);

		mMenu.add(miOpciones);
		mMenu.add(miSalir);

		mBarra.add(mMenu);

		this.setJMenuBar(mBarra);

		game = new Juego(this);
		this.add(game, BorderLayout.CENTER);

		tJugar = new Timer(10, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				game.repaint();
				game.mover();
				if (game.destruidos == game.bloques.size()) {
					nivel++;
					if (nivel == 6) {
						JOptionPane
								.showMessageDialog(
										VentanaJuego.this,
										"Lo sentimos pero no quedan mas niveles en la demo",
										"Fin de demo",
										JOptionPane.INFORMATION_MESSAGE);
						GameOver();
					} else {
						VentanaJuego.this.remove(game);
						game = new Juego(VentanaJuego.this);
						VentanaJuego.this.add(game, BorderLayout.CENTER);
						VentanaJuego.this.repaint();
						game.repaint();
						game.requestFocus();
					}
				}
				try {
					if (!VentanaJuego.this.getFocusOwner().equals(game)
							&& !game.isInicio()) {
						game.setPausa(true);
					}
				} catch (NullPointerException e) {
					if (!game.isInicio()) {
						game.setPausa(true);
					}
				}
			}
		});

		this.setSize(game.getWidth(), game.getHeight() + 20);
		tJugar.start();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == miOpciones) {
			Secundario s = new Secundario(this);
			s.pack();
			s.setVisible(true);
		}
		if (e.getSource() == miSalir) {
			this.GameOver();
		}
	}

	/**
	 * Comprueba si la puntuacion entra en el ranking,finaliza la ventana de
	 * juego y vuelve al menu
	 * 
	 */
	public void GameOver() {
		Sonidos.FONDO.stop();
		Sonidos.GAMEOVER.play();
		int puesto = -1;
		Scanner sc;
		puntuaciones = new ArrayList<String>();
		try {
			File f = new File("maxpunt.txt");
			if (!f.exists()) {
				f.createNewFile();
				RandomAccessFile fCopiar = new RandomAccessFile(f, "rw");
				File fOrigen = new File(this.getClass()
						.getResource("maxpunt.txt").toURI());
				RandomAccessFile fLectura = new RandomAccessFile(fOrigen, "r");
				for (int i = 0; i < fOrigen.length(); i++) {
					Byte bl = fLectura.readByte();
					fCopiar.writeByte(bl);
				}
				fCopiar.close();
				fLectura.close();
			}
			sc = new Scanner(f);
			while (sc.hasNext()) {
				puntuaciones.add(sc.nextLine());
			}
			for (int i = puntuaciones.size() - 1; i >= 0; i--) {
				int puntos = Integer.parseInt(puntuaciones.get(i).substring(
						puntuaciones.get(i).indexOf('.') + 1));
				if (puntos < getPuntuacion()) {
					puesto = i + 1;
				}
			}
			if (puesto != -1) {

				do {
					nombre = JOptionPane
							.showInputDialog(
									this,
									String.format(
											"Enhorabuena has conseguido el %dº puesto.Introduce tu nombre",
											puesto));
					if (nombre.length() > 10) {
						JOptionPane
								.showMessageDialog(
										this,
										"EL nombre debe ser de 10 carácteres como máximo",
										"Error", JOptionPane.ERROR_MESSAGE);
					}
				} while (nombre.length() > 10);
				if (!nombre.equals(null)) {
					puntuaciones.add(puesto - 1,
							String.format("%s.%d", nombre, getPuntuacion()));
					puntuaciones.remove(puntuaciones.size() - 1);
					PrintWriter w = new PrintWriter(f);
					for (int i = 0; i < puntuaciones.size(); i++) {
						w.println(puntuaciones.get(i));
					}
					w.close();
				}
			} else
				JOptionPane
						.showMessageDialog(this,
								"Lo siento pero no ha conseguido entrar en la clasificación");

		} catch (Exception e) {
			System.err.println("Error crítico:" + e.getMessage());
		}
		menu.setVisible(true);
		dispose();
	}

	public int getNivel() {
		return nivel;
	}

	public int getPuntuacion() {
		return puntuacion;
	}

	public int getVidas() {
		return vidas;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;

	}
}
