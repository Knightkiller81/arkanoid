package juego;

import java.applet.Applet;
import java.applet.AudioClip;

/**
 * Clase que contiene los sonidos del juego
 * 
 * @author Tor
 * @version 1.0 02/04/2014
 */
public class Sonidos {
	public static final AudioClip FONDO = Applet.newAudioClip(Sonidos.class
			.getResource("fondo.wav"));
	public static final AudioClip REBOTE = Applet.newAudioClip(Sonidos.class
			.getResource("rebote.wav"));
	public static final AudioClip GAMEOVER = Applet.newAudioClip(Sonidos.class
			.getResource("gameover.wav"));
	public static final AudioClip LEVELUP = Applet.newAudioClip(Sonidos.class
			.getResource("lvlup.wav"));
}
