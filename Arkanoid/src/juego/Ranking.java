package juego;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.util.Scanner;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * JDialog modal que muestra las puntuaciones máximas guardadas
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 */
public class Ranking extends JDialog {
	private JLabel[] lblPuntuaciones;
	private JLabel lblNombre;
	private JLabel lblPuntuacion;
	private static final int X = 50;
	private int y = 80;
	private static final Color[] COLORES={Color.WHITE,Color.MAGENTA,Color.RED,Color.YELLOW,Color.BLUE,Color.ORANGE,Color.CYAN,Color.GREEN,Color.PINK,Color.GRAY};

	public Ranking(MenuPrincipal mp) {
		super(mp, true);
		try {
			this.setTitle("Maximas Puntuaciones");
			this.setResizable(false);
			this.setLayout(null);
			this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			File f = new File("maxpunt.txt");
			if (!f.exists()) {
				f.createNewFile();
				RandomAccessFile fCopiar = new RandomAccessFile(f, "rw");
				File fOrigen = new File(this.getClass()
						.getResource("maxpunt.txt").toURI());
				RandomAccessFile fLectura = new RandomAccessFile(fOrigen, "r");
				for (int i = 0; i < fOrigen.length(); i++) {
					Byte bl = fLectura.readByte();
					fCopiar.writeByte(bl);
				}
				fCopiar.close();
				fLectura.close();
			}
			
			InputStream archivofuente = this.getClass().getResourceAsStream(
					"puntuaciones.ttf");
			
			Font uniFont = Font.createFont(Font.TRUETYPE_FONT, archivofuente);
			Font fuente = uniFont.deriveFont(24f);

			lblNombre = new JLabel("Nombre");
			lblNombre.setLocation(60, 30);
			lblNombre.setFont(fuente);
			lblNombre.setForeground(Color.WHITE);
			lblNombre.setSize(lblNombre.getPreferredSize());
			this.add(lblNombre);

			lblPuntuaciones = new JLabel[10];
			Scanner sc = new Scanner(f);
			for (int i = 0; i < lblPuntuaciones.length; i++) {
				String linea = sc.nextLine();
				String nombre = linea.substring(0, linea.indexOf('.'));
				String puntos = linea.substring(linea.indexOf('.') + 1);
				lblPuntuaciones[i] = new JLabel(String.format("%2d-%s%"
						+ (20 - nombre.length()) + "s", i + 1, nombre, puntos));
				lblPuntuaciones[i].setFont(fuente);
				lblPuntuaciones[i].setForeground(COLORES[i]);
				lblPuntuaciones[i].setLocation(X, y);
				lblPuntuaciones[i].setSize(lblPuntuaciones[i]
						.getPreferredSize());
				y += lblPuntuaciones[i].getHeight() + 10;
				this.add(lblPuntuaciones[i]);
			}
			sc.close();
			lblPuntuacion = new JLabel("Puntuacion");
			lblPuntuacion.setFont(fuente);
			lblPuntuacion.setForeground(Color.WHITE);
			lblPuntuacion.setSize(lblPuntuacion.getPreferredSize());
			lblPuntuacion.setLocation(lblPuntuaciones[0].getX()
					+ lblPuntuaciones[0].getWidth() - lblPuntuacion.getWidth()
					/ 2, 30);
			this.getContentPane().setBackground(Color.black);
			this.add(lblPuntuacion);
		} catch (IOException | URISyntaxException e) {
			JOptionPane.showMessageDialog(this, "Error al cargar puntuaciones"
					+ e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			this.dispose();
		} catch (FontFormatException e) {
			System.err.println("Problema de fuentes");
		}
	}
}
