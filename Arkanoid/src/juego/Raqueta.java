package juego;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Clase de la raqueta del juego
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 */
public class Raqueta extends Colisionable {
	private int xa;
	/**
	 * Genera la raqueta en el juego
	 * @param game Juego en el que se va a generar la raqueta
	 */
	public Raqueta(Juego game) {
		this.game = game;
		x = 40;
		xa = 0;
		this.width = 50;
		this.height = 10;
		y = game.getHeight() - 90;
	}

	public Juego getGame() {
		return game;
	}

	public int getXa() {
		return xa;
	}

	@Override
	public int getY() {
		return y;
	}

	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			xa = 5;
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			xa = -5;
	}

	public void keyReleased(KeyEvent e) {
		xa = 0;
	}

	public void mouseMoved(MouseEvent e) {
		if (e.getX() + this.getWidth() < game.getWidth() && !game.isPausa()) {
			x = e.getX();
		}
	}
	/**
	 * Maneja el movimiento de la raqueta
	 */
	public void move() {
		if (x + xa > 0 && x + xa < game.getWidth() - width)
			x += xa;
	}

	public void paint(Graphics2D g) {
		g.fillRect(x, y, width, height);
	}

	public void setGame(Juego game) {
		this.game = game;
	}

	public void setXa(int xa) {
		this.xa = xa;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}
}
