package juego;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * Clase que controla la forma,el movimiento y posicion de la bola asi como sus
 * colisiones
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 */
public class Bola {
	private int x;
	private int y;
	private int xa;
	private int ya;
	private static final int DIAMETRO = 10;
	private boolean rebote;
	private Juego game;
	/**
	 * Crea una pelota de diametro 10 en medio de la pantalla
	 * @param game Juego en el cual se creará la pelota
	 */
	public Bola(Juego game) {
		this.game = game;
		x = game.getWidth() / 2;
		y = game.getHeight() / 2;
		xa = 0;
		ya = 0;
	}


	public int getX() {
		return x;
	}

	public int getXa() {
		return xa;
	}

	public int getYa() {
		return ya;
	}

	/**
	 * Genera el hitbox de la bola en base a sus parametros
	 * 
	 * @return Un rectangulo generado en base al valor de x,y y el diametro de
	 *         la bola
	 */
	Rectangle hitbox() {
		return new Rectangle(x, y, DIAMETRO, DIAMETRO);
	}

	/**
	 * Detecta por que lado del objeto esta colisionando la bola
	 * 
	 * @param col el objeto con el que colisiona la bola
	 */
	private void ladocolision(Colisionable col) {
		// Esquina superior izquierda
		if (this.x < col.getX() && this.y < col.getY()) {
			ya = -game.getVelocidad();
			xa = -game.getVelocidad();
		}

		// Esquina superior derecha
		else if (this.y < col.getY()
				&& this.x + this.hitbox().width > col.getX() + col.getWidth()) {
			ya = -game.getVelocidad();
			xa = game.getVelocidad();
		}
		// Parte superior
		else if (this.y < col.getY() && this.x < col.getX() + col.getWidth()) {
			ya = -game.getVelocidad();
		}

		// Lado derecho
		else if (this.x > col.getX()
				&& this.y + Bola.DIAMETRO < col.getY() + col.getHeight()) {
			xa = game.getVelocidad();
		}
		// Esquina inferior derecha
		else if (this.x > col.getX() + col.getWidth() && this.y > col.getY()) {
			ya = game.getVelocidad();
			xa = game.getVelocidad();
		}
		// Abajo
		else if (this.y > col.getY() && this.x > col.getX()) {
			ya = game.getVelocidad();
		}
		// Esquina inferior izquierda
		else if (this.y > col.getY() && this.x < col.getX()) {
			xa = -game.getVelocidad();
			ya = game.getVelocidad();
		}
		// Lado izquierdo
		else if (this.x < col.getX() && this.y < col.getY() + col.getHeight()) {
			xa = -game.getVelocidad();
		}
		if (col.getClass().equals(Bloque.class)) {
			Bloque bloque = (Bloque) col;
			bloque.setGolpe(bloque.getGolpe() - 1);
			if(bloque.isDestruido()){
				game.destruidos += 1;
			}
		}

	}

	/**
	 * Controla el movimiento de la bola segun sus colisones
	 */
	public void move() {
		rebote = true;
		if (x + xa < 0)
			xa = game.getVelocidad();
		else if (x + xa > game.getWidth() - 15)
			xa = -game.getVelocidad();
		else if (y + ya < 0)
			ya = game.getVelocidad();
		else if (y + ya > game.getHeight() - DIAMETRO) {
			if (game.ventana.getVidas() == 0)
				game.ventana.GameOver();
			else {
				game.ventana.setVidas(game.ventana.getVidas() - 1);
				game.lblVidas.setText(String.format("Vidas:%d",
						game.ventana.getVidas()));
				game.setInicio(true);
				x = game.getWidth() / 2;
				y = game.getHeight() / 2;
				xa = 0;
				ya = 0;
			}
		} else if (game.raqueta.colision()) {
			ladocolision(game.raqueta);
		} else {
			for (Bloque bl : game.bloques) {
				if (bl.colision()) {
					game.ventana
							.setPuntuacion(game.ventana.getPuntuacion() + 1);
					game.lblPuntuacion.setText(String.format("Puntuacion:%d",
							game.ventana.getPuntuacion()));
					ladocolision(bl);
				}
			}
			rebote = false;
		}

		if (rebote) {
			if (Opciones.sonido) {
				Sonidos.REBOTE.play();
			}
		}
		x = x + xa;
		y = y + ya;

	}

	public void paint(Graphics2D g) {
		g.setColor(Color.BLUE);
		g.fillOval(x - 1, y - 1, DIAMETRO + 2, DIAMETRO + 2);
	}
	/**
	 * Fija la aceleracion de la bola horizontal
	 * @param xa aceleracion de la bola horizontal
	 */
	public void setXa(int xa) {
		this.xa = xa;
	}
	/**
	 * Fija la aceleracion de la bola vertical
	 * @param ya aceleracion de la bola vertical
	 */
	public void setYa(int ya) {
		this.ya = ya;
	}
}
