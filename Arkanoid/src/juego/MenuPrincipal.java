package juego;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Menu principal del juego
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 */
public class MenuPrincipal extends JFrame implements ActionListener {
	private JButton btnNuevaPartida;
	private JButton btnOpciones;
	private JButton btnMaximasPuntuaciones;
	private JLabel lblTitulo;
	private ImageIcon titulo;
	private ImageIcon fondo;
	private JPanel panelBotones;
	private JPanel panelPuntuaciones;
	private JLabel lblFondo;
	
	/**
	 * Crea un menu principal con 3 botones,un titulo y un fondo 
	 */
	public MenuPrincipal() {
		super("Arkanoid 3000");
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		titulo = new ImageIcon(this.getClass().getResource("titulo.png"));
		fondo = new ImageIcon(this.getClass().getResource("background.gif"));

		lblFondo = new JLabel(fondo);
		this.add(lblFondo);

		lblFondo.setLayout(new BorderLayout());

		lblTitulo = new JLabel(titulo);
		lblFondo.add(lblTitulo, BorderLayout.NORTH);

		panelBotones = new JPanel();
		panelBotones.setLayout(null);

		btnNuevaPartida = new JButton("Nueva partida");
		InputStream archivofuente = this.getClass().getResourceAsStream(
				"REDCIRCL.ttf");
		Font uniFont;
		try {
			uniFont = Font.createFont(Font.TRUETYPE_FONT, archivofuente);
			Font f = uniFont.deriveFont(24f);
			btnNuevaPartida.setFont(f);
			btnNuevaPartida.setForeground(Color.RED);
			btnNuevaPartida.setOpaque(false);
			btnNuevaPartida.setBorderPainted(false);
			btnNuevaPartida.setBounds(fondo.getIconWidth() / 2 - 412 / 2, 10,
					412, 39);
			btnNuevaPartida.addActionListener(this);
			panelBotones.add(btnNuevaPartida);

			btnOpciones = new JButton("Opciones");
			btnOpciones.addActionListener(this);
			btnOpciones.setFont(f);
			btnOpciones.setForeground(Color.BLUE);
			btnOpciones.setOpaque(false);
			btnOpciones.setBorderPainted(false);
			btnOpciones.setBounds(btnNuevaPartida.getX(),
					btnNuevaPartida.getY() + btnNuevaPartida.getHeight() + 20,
					412, 39);
			panelBotones.add(btnOpciones);

			btnMaximasPuntuaciones = new JButton("Maximas Puntuaciones");
			btnMaximasPuntuaciones.addActionListener(this);
			btnMaximasPuntuaciones.setFont(f);
			btnMaximasPuntuaciones.setForeground(Color.BLACK);
			btnMaximasPuntuaciones.setOpaque(false);
			btnMaximasPuntuaciones.setBorderPainted(false);
			btnMaximasPuntuaciones.setBounds(btnOpciones.getX(),
					btnOpciones.getY() + btnOpciones.getHeight() + 25, 412, 39);
			panelBotones.add(btnMaximasPuntuaciones);

			panelBotones.setOpaque(false);

			panelPuntuaciones = new JPanel();
			panelPuntuaciones.setLayout(null);

			lblFondo.add(panelBotones, BorderLayout.CENTER);
		} catch (FontFormatException e) {
			System.err.println("Error de fuentes");
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.pack();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnNuevaPartida) {
			this.setVisible(false);
			VentanaJuego game = new VentanaJuego(this);
			game.setVisible(true);
		}
		if (e.getSource() == btnOpciones) {
			Secundario s = new Secundario(this);
			s.pack();
			s.setVisible(true);
		}
		if (e.getSource() == btnMaximasPuntuaciones) {
			Ranking r = new Ranking(this);
			r.setSize(630, 500);
			r.setVisible(true);
		}

	}
}
