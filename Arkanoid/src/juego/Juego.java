package juego;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Panel que contiene el juego
 * 
 * @author tor
 * @version 1.0 19/06/2014
 */
public class Juego extends JPanel {
	Bola bola;
	Raqueta raqueta;
	ArrayList<Bloque> bloques;
	private boolean pausa;
	private boolean inicio;
	private int velocidad;
	public int destruidos = 0;
	VentanaJuego ventana;
	JLabel lblPuntuacion;

	private JLabel lblNivel;

	JLabel lblVidas;
	/**
	 * Crea un nuevo panel de juego
	 * @param ventana en la cual se va a colocar el panel de juego
	 */
	public Juego(VentanaJuego ventana) {
		this.setLayout(null);
		inicio = true;
		this.setSize(800, 700);
		if (ventana.getNivel() < 4)
			this.setVelocidad(2);
		else
			this.setVelocidad(3);
		this.ventana = ventana;
		raqueta = new Raqueta(this);

		bola = new Bola(this);

		this.addKeyListener(new KeyHandler());
		GeneradorNivel glevel;
		try {
			InputStream archivofuente = this.getClass().getResourceAsStream(
					"puntuaciones.ttf");

			Font uniFont = Font.createFont(Font.TRUETYPE_FONT, archivofuente);
			Font fuente = uniFont.deriveFont(10f);
			glevel = new GeneradorNivel(this,this.getClass()
							.getResourceAsStream("nivel" + ventana.getNivel() + ".txt"));
			bloques = glevel.getBloques();
			BufferedImage cursorImg = new BufferedImage(16, 16,
					BufferedImage.TYPE_INT_ARGB);

			Cursor blankCursor = Toolkit.getDefaultToolkit()
					.createCustomCursor(cursorImg, new Point(0, 0),
							"blank cursor");

			this.setCursor(blankCursor);

			lblPuntuacion = new JLabel(String.format("Puntuacion:%d",
					ventana.getPuntuacion()));
			lblPuntuacion.setFont(fuente);
			lblPuntuacion.setBounds(
					350 - lblPuntuacion.getPreferredSize().width, 650,
					lblPuntuacion.getPreferredSize().width + 20,
					lblPuntuacion.getPreferredSize().height);
			this.add(lblPuntuacion);

			lblNivel = new JLabel(String.format("Nivel:%d", ventana.getNivel()));
			lblNivel.setFont(fuente);
			lblNivel.setBounds(lblPuntuacion.getX() + lblPuntuacion.getWidth()
					+ 40, 650, lblNivel.getPreferredSize().width,
					lblNivel.getPreferredSize().height);
			this.add(lblNivel);

			lblVidas = new JLabel(String.format("Vidas:%d", ventana.getVidas()));
			lblVidas.setBounds(lblNivel.getX() + lblNivel.getWidth() + 40, 650,
					lblVidas.getPreferredSize().width + 10,
					lblVidas.getPreferredSize().height);
			lblVidas.setFont(fuente);
			this.add(lblVidas);

			this.addMouseMotionListener(new MouseHandler());
			this.setFocusable(true);
			reprfondo();
		} catch (FontFormatException e) {
			System.err.println("Problema con la fuente");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Problema con la fuente");
		}

	}

	/**
	 * Aumenta la velocidad del juego
	 */
	public void aumentarVelocidad() {
		this.setVelocidad(this.getVelocidad() + 1);
	}

	public int getVelocidad() {
		return velocidad;
	}
	/**
	 * Marca si la bola esta parada o no(Inicio del juego o perdida de una vida)
	 * @return true si la bola esta parada,false en caso contrario
	 */
	public boolean isInicio() {
		return inicio;
	}
	/**
	 * Marca si el juego esta pausado o no
	 * @return true si el juego esta pausado,false en caso contrario
	 */
	public boolean isPausa() {
		return pausa;
	}

	/**
	 * Mueve la raqueta y la bola a las posiciones correspondientes.
	 * @see Raqueta
	 * @see Bola
	 */
	public void mover() {
		if (!pausa) {
			bola.move();
			raqueta.move();
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		bola.paint(g2d);
		raqueta.paint(g2d);
		for (Bloque bl : bloques) {
			bl.paint(g2d);

		}
		if (pausa) {
			Font fpausa = new Font("Verdana", Font.BOLD, 30);
			g2d.setColor(Color.RED);
			g2d.setFont(fpausa);
			g2d.drawString("En pausa", this.getWidth() / 2 - 30,
					this.getHeight() / 2 - 30);
			g2d.setColor(Color.BLUE);

		}
		if (inicio) {
			g2d.setColor(Color.BLACK);
			g2d.drawString("Presiona SPACE para comenzar ",
					this.getWidth() / 2 - 50, this.getHeight() / 2 - 50);
		}
	}

	/**
	 * Reproduce el sonido de fondo si el handler de opciones esta a true
	 */
	private void reprfondo() {
		if (Opciones.sonido) {
			Sonidos.FONDO.loop();
		}
	}

	public void setInicio(boolean inicio) {
		this.inicio = inicio;
	}

	public void setPausa(boolean pausa) {
		this.pausa = pausa;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	/**
	 * Controla los eventos de teclado (movimiento de raqueta y pausa)
	 * @author tor
	 */
	private class KeyHandler extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_P) {
				if (pausa == false)
					pausa = true;
				else
					pausa = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_SPACE && inicio) {
				inicio = false;
				bola.setYa(velocidad);
				bola.setXa(velocidad);
			}
			if (Opciones.handler) {
				raqueta.keyPressed(e);
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if (Opciones.handler) {
				raqueta.keyReleased(e);
			}

		}
	}
	/**
	 * Controla los eventos de raton(movimiento de la raqueta con el raton)
	 * @author tor
	 *
	 */
	private class MouseHandler extends MouseAdapter {

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			if (!Opciones.handler) {
				Juego.this.raqueta.mouseMoved(e);
			}
		}

	}
}
