package juego;

import java.awt.Rectangle;

/**
 * Clase para los objetos que pueden colisionar con la bola
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 */
public class Colisionable {

	protected int x;
	protected int y;
	protected int width;
	protected int height;
	protected Juego game;

	/**
	 * Comprueba si el objeto colisionable entra en colision con la bola en el
	 * juego
	 * 
	 * @return true si colisiona,false en caso negativo
	 */
	public boolean colision() {
		return game.bola.hitbox().intersects(this.hitbox());
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	/**
	 * Crea el hitbox del objeto en base a sus parametros
	 * 
	 * @return Un rectangulo en base a los parametros x,y,width,height del
	 *         objeto
	 */
	public Rectangle hitbox() {
		return new Rectangle(x, y, width, height);
	}
	
	public void setHeight(int height) {
		this.height = height;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

}
