package juego;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * JDialog modal que controla el cambio en las opciones de juego
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 * @see Opciones
 */
public class Secundario extends JDialog implements ItemListener {
	private JComboBox<String> cbHandler;
	private JCheckBox chkbSonido;
	private JFrame ventana;
	/**
	 * Genera el modal con las opciones
	 * @param ventana JFrame del cual es modal
	 */
	public Secundario(JFrame ventana) {
		super(ventana, true);
		this.setTitle("Opciones");
		this.setLayout(new FlowLayout());
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		this.ventana = ventana;

		String[] handlers = { "Teclado", "Ratón" };

		cbHandler = new JComboBox<String>(handlers);
		cbHandler.addItemListener(this);
		if (Opciones.handler) {
			cbHandler.setSelectedIndex(0);
		} else {
			cbHandler.setSelectedIndex(1);
		}
		this.add(cbHandler);

		chkbSonido = new JCheckBox("Sonido");
		if (Opciones.sonido) {
			chkbSonido.setSelected(true);
		} else {
			chkbSonido.setSelected(false);
		}
		chkbSonido.addItemListener(this);
		this.add(chkbSonido);
		this.setLocationRelativeTo(ventana);
	}
	/**
	 * Controla los cambios en la JComboBox
	 */
	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == cbHandler) {
			if (cbHandler.getSelectedItem().equals("Teclado")) {
				Opciones.handler = true;
			} else {
				Opciones.handler = false;
			}
		}
		if (e.getSource() == chkbSonido) {
			if (chkbSonido.isSelected()) {
				Opciones.sonido = true;
				if (ventana.getClass().equals(VentanaJuego.class)) {
					Sonidos.FONDO.loop();
				}
			} else {
				Opciones.sonido = false;
				Sonidos.FONDO.stop();
			}
		}
	}
}
