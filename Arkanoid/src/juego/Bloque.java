package juego;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * Clase para los bloques del arkanoid,hereda de la clase colisionable
 * 
 * @author Tor
 * @version 1.0 19/06/2014
 */
public class Bloque extends Colisionable {
	private Rectangle hitbox;
	private boolean destruido;
	private int golpe;
	private static final Color[] COLORES = { Color.YELLOW, Color.GREEN,
			Color.GRAY };
	/**
	 * Crea un bloque de 80x20
	 * @param game Juego en el cual se creará el bloque
	 * @param x Coordenada x en la cual se creará el bloque
	 * @param y Coordenada y en la cual se creará el bloque
	 * @param golpe Cantidad de veces que tiene que colisionar el bloque con la bola para ser destruido
	 */
	public Bloque(Juego game, int x, int y, int golpe) {
		this.game = game;
		this.x = x;
		this.y = y;
		this.width = 80;
		this.height = 20;
		hitbox = new Rectangle(x, y, width, height);
		this.golpe = golpe;

	}

	/**
	 * Comprueba si el bloque esta colisionando con la bola y no esta destruido
	 * 
	 * @return True si la bola colisiona con el bloque y el bloque no esta
	 *         destruido False si no colisiona o el bloque esta destruido
	 */
	@Override
	public boolean colision() {
		return this.isDestruido() ? false : game.bola.hitbox().intersects(
				this.hitbox);
	}

	public int getGolpe() {
		return golpe;
	}

	@Override
	public int getHeight() {
		return height;
	}

	/**
	 * Comprueba si los golpes estan a 0
	 * 
	 * @return Si los golpes estan a 0 devuelve true,si no devuelve false
	 */
	public boolean isDestruido() {
		if (golpe == 0) {
			setDestruido(true);
		}
		return destruido;
	}

	public void paint(Graphics2D g) {
		if (!this.isDestruido()) {
			g.setColor(COLORES[golpe - 1]);
			g.fill(hitbox);
			g.setColor(Color.BLACK);
			g.draw(hitbox);
			g.setColor(Color.BLUE);
		}
	}

	public void setDestruido(boolean isDestruido) {
		this.destruido = isDestruido;
	}

	public void setGolpe(int golpe) {
		this.golpe = golpe;
	}

	@Override
	public void setHeight(int height) {
		this.height = height;
	}
}
